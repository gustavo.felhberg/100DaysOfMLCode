# 100 Days Of ML - LOG

## Day 0 : July 15 , 2018
 
**Today's Progress** : Decided to finally start this challenge! To warm up, I am going to start with a Kaggle competition for the first 30 days (which is also the deadline) and for the rest of the 100 days I'll focus in a different project (TBD)

Competition: https://www.kaggle.com/c/home-credit-default-risk

Repo: https://gitlab.com/gustavo.felhberg/kaggle_home_credit_default_risk

**Thoughts** : Getting exited. Hope to not give up in the middle of this. :)